1. Observer pattern là một mẫu thiết kế phần mềm mà một đối tượng, gọi là subject, duy trì một danh sách các thành phần phụ thuộc nó, 
gọi là observer, và thông báo tới chúng một cách tự động về bất cứ thay đổi nào, thường thì bằng cách gọi 1 hàm của chúng.

Mẫu thiết kế này rất hữu ích trong việc xây dựng các ứng dụng theo mô hình dựa trên sự kiện (event-driven) hoặc các ứng dụng liên tục cần cập nhật trạng thái.

2. Mục tiêu
Định nghĩa mối phụ thuộc một - nhiều giữa các đối tượng để khi mà một đối tượng có sự thay đổi trạng thái, tất các thành phần phụ thuộc của nó sẽ được thông báo và cập nhật một cách tự động.
Một đối tượng có thể thông báo đến một số lượng không giới hạn các đối tượng khác.

3. Observer Pattern được áp dụng khi:

Sự thay đổi trạng thái ở 1 đối tượng có thể được thông báo đến các đối tượng khác mà không phải giữ chúng liên kết quá chặt chẽ
Cần mở rộng dự án với ít sự thay đổi nhất.