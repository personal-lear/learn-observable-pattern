import { Component } from '@angular/core';
import {TestServiceService} from "../../test-service.service";

@Component({
  selector: 'app-test1',
  standalone: true,
  imports: [],
  templateUrl: './test1.component.html',
  styleUrl: './test1.component.scss'
})
export class Test1Component {
  currentState: string = '';

  constructor(private stateService: TestServiceService) {}

  ngOnInit() {
    // Đăng ký nhận thông báo về các thay đổi trạng thái từ service
    this.stateService.subscribe((state: string) => {
      console.log(state)
      this.currentState = state;
    });
  }

  changeData(){
    this.stateService.updateState('test1');
  }
}
