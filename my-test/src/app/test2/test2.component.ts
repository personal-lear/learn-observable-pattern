import { Component } from '@angular/core';
import {TestServiceService} from "../../test-service.service";

@Component({
  selector: 'app-test2',
  standalone: true,
  imports: [],
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.scss'
})
export class Test2Component {
  currentState: string = '';

  constructor(private stateService: TestServiceService) {}

  ngOnInit() {
    // Đăng ký nhận thông báo về các thay đổi trạng thái từ service
    this.stateService.subscribe((state: string) => {
      console.log(state)
      this.currentState = state;
    });
  }
  changeData(){
    this.stateService.updateState('test2');
  }

}
