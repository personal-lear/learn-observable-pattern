import { Injectable } from '@angular/core';
import {Subject, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestServiceService {
  private stateSubject = new Subject<string>();

  // Phương thức để cập nhật trạng thái và thông báo cho các subscriber
  updateState(newState: string) {
    console.log(123)
    this.stateSubject.next(newState);
  }

  subscribe(callback: (state: string) => void): Subscription {
    return this.stateSubject.subscribe(callback);
  }
}
